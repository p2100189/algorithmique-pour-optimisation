1-
Proposition algo:

int prop_algo(int k, int n){
    if(k==n || k==0)
        return 1;
    else
        return prop_algo(k,n-1)+prop_algo(k-1,n-1);
}

Solution juste mais cela est récursif naif, on doit utiliser de la programmation dynamique pour
une solution optimale

Pour le visualiser faire une Table avec les n en haut et les k à gauche et remplir en faisant la
suite

Correction:
Créer une table T de taille k*n
Ensuite 
pour i allant de 0 à n
    T[0,i]=1
pour j allant de 0 à k
    T[j,j]=1

Pour i allant de 1 à k
    Pour j allant de i+1 à n
        T[i,j]=T[i,j-1]+T[i-1,j-1]

Retourner T[k,n]
        

Complexité:
Création de la table: compléxité k*n<=n²  donc O(n²)
2 premiere boucles:O(n)+O(k) donc O(n) 
double boucle:O(k*n)   évidemment O(n²)
Total:O(n²)


2-
Dans l'algo de la question 1, la complexité en mémoire est la taille de la table, c'est à
dire n*k donc environ n²
Pour passer à une complexité spatiale O(n) on peut ne pas créer la table en entière mais juste la
dernière ligne nécessaire (ligne n-2 étant inintéressante pour n juste n et n-1 nécessaire)

Algo:
    Créer ligne A de taille n 
    et ligne B de taille n 
    Pour i allant de 0 à n
        A[i]=1
    Pour j allant de 1 à k 
        B[j]=1
        Pour i allant de j à n
            B[i]=B[i-1]+A[i-1]
        A <- B 
        Videz B 

Algo pas forcément sur, l'important est la compréhension dans le tableau,
le fait de vider la ligne n-1