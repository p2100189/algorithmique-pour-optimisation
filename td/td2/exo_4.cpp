#include <iostream>
using namespace std;

int prop_algo(int k, int n){
    if(k==n || k==0)
        return 1;
    else
        return prop_algo(k,n-1)+prop_algo(k-1,n-1);
}


int main()
{
    cout << "Donnez le k et n voulu"<<endl;
    int k,n;
    cin >> k >> n;
    while(k>n){
        cout << "k doit être inférieur ou égal à n"<<endl;
        cin >> k >> n;
    }
   cout <<"Réponse : "<< prop_algo(k,n) << endl;
    return 0;
}