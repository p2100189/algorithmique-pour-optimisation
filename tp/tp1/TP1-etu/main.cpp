#include "graph.hpp"
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <random>
#include <unordered_set>

using namespace std;

/******************/
/* SPECIAL GRAPHS */
/******************/

Graph clique(int n) {
    Graph retour(n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            retour.add_edge(i, j);
        }
    }
    return retour;
}

Graph biclique(int n, int p) {
    Graph retour(n + p);
    for (int i = 0; i < n; i++) {
        for (int j = n; j < n + p; j++) {
            retour.add_edge(i, j);
        }
    }
    return retour;
}

/****************/
/* ALGO GLOUTON */
/****************/

// algo glouton
unordered_set<int> greedy_det(Graph & G) {
    unordered_set<int> retour;
    retour.emplace(0);

    // Parcourir tous les autres sommets
    for (int i = 1; i < G.get_size(); i++) {
        bool adjacent = false; // Vérifie si i est adjacent à un sommet déjà dans retour

        // Vérifier si le sommet i est adjacent à un sommet dans l'ensemble retour
        for (int v : retour) {
            if (G.has_edge(i, v)) {
                adjacent = true;
                break;
            }
        }

        // Si i n'est adjacent à aucun sommet de retour, on l'ajoute
        if (!adjacent) {
            retour.emplace(i);
        }
    }
    return retour;
}

/**************/
/* ALGO EXACT */
/**************/

// Fonction auxiliaire
unordered_set<int> exact_aux(Graph & g, vector<bool> & is_free, int i) {
    unordered_set<int> retour;

    // Condition d'arrêt : Si i est plus grand ou égal à la taille du graphe, retournez un ensemble vide
    if (i >= g.get_size()) {
        return retour;
    }

    // 1. Cas où nous excluons le sommet i (G \ i)
    unordered_set<int> exclure_i = exact_aux(g, is_free, i + 1);

    // 2. Cas où nous incluons le sommet i (G_i), en vérifiant si c'est valide
    unordered_set<int> inclure_i;
    if (is_free[i]) {
        bool adjacent = false;
        // Vérifier si i est adjacent à un sommet déjà dans l'ensemble actuel exclure_i
        for (int v : exclure_i) {
            if (g.has_edge(i, v)) {
                adjacent = true;  // i est adjacent à un sommet déjà présent
                break;
            }
        }
        if (!adjacent) {
            // Si i n'est pas adjacent à un sommet de exclure_i, on peut l'inclure
            inclure_i = exact_aux(g, is_free, i + 1);
            inclure_i.emplace(i);  // Ajouter i à l'ensemble
        }
    }

    // 3. Retourner l'ensemble le plus grand entre exclure_i et inclure_i
    if (inclure_i.size() > exclure_i.size()) {
        retour = inclure_i;
    } else {
        retour = exclure_i;
    }

    return retour;
}


// Algorithme exact
unordered_set<int> exact(Graph & g) {
    vector<bool> is_free(g.get_size(), true);  // Par défaut, tous les sommets sont libres
    return exact_aux(g, is_free, 0);
}


/********/
/* MAIN */
/********/

int main() {
    srand(time(NULL));
    Graph g = Graph("data/graphEL_toygraph");
    Graph g2 = Graph("data/graphEL_rand_100_8");
    Graph graph_clique = clique(10);
    Graph graph_biclique = biclique(5, 5);
    // g.set_int_mark(0, 10);
    // g.set_shape_mark(3, Graph::TRIANGLE);
    // g.set_color_mark(4, Graph::RED);
    // g.print();
    g.show();
    unordered_set<int> res=exact(g);

    for(int v : res)
    {
        std::cout<<v;
    }

    // graph_biclique.show();

    /*
    unordered_set<int> is = greedy(g,10);
    cout << is.size() << endl;
    is = RL(g,10);
    cout << is.size() << endl;
    is = RS(g,10);
    cout << is.size() << endl;
    is = RL2(g,10);
    cout << is.size() << endl;
    */
    return 0;
}
