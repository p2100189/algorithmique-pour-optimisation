#include "graph.hpp"
#include <cassert>
#include <climits>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <random>
#include <unordered_set>

using namespace std;

Graph random_clique(int n) {
    Graph retour(n);
    srand(time(0));

    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            int poids = rand() % n + 1;
            retour.add_edge(i, j, poids);
        }
    }
    return retour;
}

/******************/
/* Floyd-Warshall */
/******************/

/** Relation de récurrence :
 * D[i][j][k]=min(D[i][j][k−1], D[i][k][k−1]+D[k][j][k−1])
 */

using namespace std;

vector<vector<int>> floyd_warshall(Graph & G) {
    int n = G.get_size();
    vector<vector<int>> distances(n, vector<int>(n, INT_MAX));

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                distances[i][j] = 0;
            } else if (G.has_edge(i, j)) {
                distances[i][j] = G.get_weight(i, j);
            }
        }
    }
    return distances;

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (distances[i][k] != INT_MAX || distances[k][j] != INT_MAX) {
                    int new_distance = distances[i][k] + distances[k][j];
                    // cout << "Trying to update distances[" << i << "][" << j << "] from "
                    //      << distances[i][j] << " to " << new_distance << endl;

                    distances[i][j] = min(distances[i][j], new_distance);
                }
            }
        }
    }

    return distances;
}

// Complexité temporelle : O(n^3) car triple boucle pour parcourir tout les sommets
// Complexité spatiale : O(n^2) car on stocke les distances entre chaque sommet dans une matrice 2d (distances)

/*******************************/
/* Floyd-Warshall avec chemins */
/*******************************/

/**
 * relation de récurrence :
 *  P[i][j][k]={
P[i][j][k−1]    si d(i,j,k) = d(i, j, k−1)
k               si d(i, j, k) > d(i, k, k−1) + d(k, j, k−1)
 */

pair<vector<vector<int>>, vector<vector<int>>> floydwarshall2(Graph & G) {
    int n = G.get_size();
    pair<vector<vector<int>>, vector<vector<int>>> res;
    res.first = vector<vector<int>>(n, vector<int>(n, INT_MAX)); // Matrice des distances
    res.second = vector<vector<int>>(n, vector<int>(n, -1));     // Matrice des prédécesseurs

    // Initialisation des matrices
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (i == j) {
                res.first[i][j] = 0;   // Distance de i à i est 0
                res.second[i][j] = -1; // Pas de prédécesseur
            } else if (G.has_edge(i, j)) {
                res.first[i][j] = G.get_weight(i, j); // Distance de i à j
                res.second[i][j] = i;                 // Le prédécesseur de j est i
            }
        }
    }

    // Algorithme de Floyd-Warshall avec mise à jour des prédécesseurs
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (res.first[i][k] != INT_MAX && res.first[k][j] != INT_MAX) {
                    if (res.first[i][j] > res.first[i][k] + res.first[k][j]) {
                        res.first[i][j] = res.first[i][k] + res.first[k][j]; // Mise à jour de la distance
                        res.second[i][j] = res.second[k][j];                 // Mise à jour du prédécesseur
                    }
                }
            }
        }
    }

    return res; // Retourne les matrices de distance et de prédécesseur
}

/**************************************/
/* Génération des plus courts chemins */
/**************************************/

vector<int> get_path(vector<vector<int>> parent, int i, int j) {
    vector<int> v; // Vecteur pour stocker le chemin

    // Si parent[i][j] est l-1, cela signifie qu'i n'y a pas de chemin
    if (parent[i][j] == -1) {
        return v; // Retourne un vecteur vide
    }

    // Utiliser une stack pour reconstituer le chemin
    stack<int> s;
    while (j != -1) {
        s.push(j);        // Empile le sommet j
        j = parent[i][j]; // Remonter au prédécesseur
    }

    // Vider la stack dans le vecteur v pour avoir le chemin dans le bon ordre
    while (!s.empty()) {
        v.push_back(s.top());
        s.pop();
    }

    return v; // Retourne le chemin
}

void Affichage_perso_graph(Graph graph_a_afficher, bool affiche_chemins) {
    graph_a_afficher.show();
    if (affiche_chemins)
        graph_a_afficher.print();
}

/********/
/* MAIN */
/********/

int main() {
    Graph g(4);
    g.add_edge(0, 1);
    g.add_edge(0, 2);
    g.add_edge(0, 3, 3);
    g.add_edge(1, 2, 6);
    g.add_edge(1, 3);
    g.add_edge(2, 3, 1);

    Graph g2 = random_clique(5);

    vector<vector<int>> return_floyd = floyd_warshall(g);



    pair<vector<vector<int>>, vector<vector<int>>> return_floyd2 = floydwarshall2(g);



    cout << endl
         << "Floyd-Warshall: " << endl;
    for (long unsigned int i = 0; i < return_floyd.size(); i++) {
        for (long unsigned int j = 0; j < return_floyd[i].size(); j++) {
            cout << return_floyd[i][j] << " ";
        }
        cout << endl;
    }

    cout << endl;
    cout << "Floyd-Warshall 2: " << endl;

    for (long unsigned int i = 0; i < return_floyd2.first.size(); i++) {
        for (long unsigned int j = 0; j < return_floyd2.first[i].size(); j++) {
            cout << return_floyd2.first[i][j] << " ";
        }
        cout << endl;
    }

    cout << endl << "Prédécesseurs: " << endl;
    for (long unsigned int i = 0; i < return_floyd2.second.size(); i++) {
        for (long unsigned int j = 0; j < return_floyd2.second[i].size(); j++) {
            cout << return_floyd2.second[i][j] << " ";
        }
        cout << endl;
    }



    Affichage_perso_graph(g, false);
    return 0;
}
