#include "graph.hpp"
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <random>
#include <unordered_set>

using namespace std;

/**************/
/* ALGO EXACT */
/**************/

// Fonction auxiliaire
unordered_set<int> exact_aux(Graph & g, vector<bool> & is_free, int i) {
    if (i == g.get_size()) {
        unordered_set<int> res = {};
        for (unsigned int j = 0; j < is_free.size(); j++)
            if (is_free.at(j))
                res.insert(j);
        return res;
    }
    if (!is_free.at(i))
        return exact_aux(g, is_free, i + 1);
    is_free.at(i) = false;
    unordered_set<int> case1 = exact_aux(g, is_free, i + 1);
    is_free.at(i) = true;
    vector<bool> copy = is_free;
    for (int j : g.neighbors(i))
        is_free.at(j) = false;
    unordered_set<int> case2 = exact_aux(g, is_free, i + 1);
    for (int j : g.neighbors(i))
        is_free.at(j) = copy.at(j);
    if (case1.size() > case2.size())
        return case1;
    else
        return case2;
}

// Algorithme exact
unordered_set<int> exact(Graph & g) {
    vector<bool> is_free(g.get_size(), true);
    return exact_aux(g, is_free, 0);
}

/****************/
/* ALGO GLOUTON */
/****************/

// algo glouton
unordered_set<int> greedy_det(Graph & G) {
    unordered_set<int> IS = {};
    for (int i : G.vertices()) {
        bool tmp = true;
        for (int j : G.neighbors(i)) {
            if (IS.find(j) != IS.end())
                tmp = false;
        }
        if (tmp)
            IS.insert(i);
    }
    return IS;
}

vector<int> gen_perm(int n) {
    vector<int> res(n, 0);
    for (int i = 0; i < n; i++)
        res.at(i) = i;
    for (int i = 0; i < n; i++) {
        int r = i + rand() % (n - i);
        int tmp = res.at(i);
        res.at(i) = res.at(r);
        res.at(r) = tmp;
    }
    return res;
}

Graph shuffle(Graph & G, vector<int> perm) {
    Graph H(G.get_size());
    for (int i = 0; i < G.get_size(); i++)
        for (int j : G.neighbors(i))
            H.add_edge(perm.at(i), perm.at(j));
    return H;
}

// lance l'algo glouton t fois en mélangeant à chaque fois les étiquettes
unordered_set<int> greedy(Graph & G, int t) {
    unordered_set<int> IS = {};
    for (int i = 0; i < t; i++) {
        Graph H = shuffle(G, gen_perm(G.get_size()));
        unordered_set<int> IS2 = greedy_det(H);
        if (IS2.size() > IS.size())
            IS = IS2;
    }
    return IS;
}

/********************/
/* RECHERCHE LOCALE */
/********************/

// choisit aléatoirement un élément dans un ensemble
int sample(unordered_set<int> & IS) {
    int index = rand() % (IS.size());
    int vtx;
    for (auto it = IS.begin(); it != IS.end(); ++it) {
        if (index == 0) {
            vtx = *it;
            break;
        }
        index--;
    }
    return vtx;
}

// fonction qui complète gloutonnement un IS en un IS max par inclusion en prenant les sommets dans allowed
void IS_completion(Graph & G, unordered_set<int> & IS, list<int> allowed) {
    // Parcourir tous les sommets de la liste `allowed`
    for (auto it = allowed.begin(); it != allowed.end(); ++it) {
        int vtx = *it; // Sommet actuel

        // Vérifier si le sommet peut être ajouté à l'ensemble indépendant
        bool canAdd = true;
        for (int neighbor : G.neighbors(vtx)) { // Parcourir les voisins du sommet
            if (IS.count(neighbor)) {           // Si un voisin est déjà dans IS
                canAdd = false;
                break;
            }
        }

        // Ajouter le sommet s'il est indépendant de ceux déjà dans IS
        if (canAdd) {
            IS.insert(vtx); // Ajouter le sommet à IS
        }
    }
}

// Une étape de la recherche locale
void RL_step(Graph & G, unordered_set<int> & IS, int vtx) {
    // Retirer le sommet vtx de l'ensemble indépendant
    IS.erase(vtx);

    // Liste des sommets autorisés pour l'ajout
    list<int> allowed;

    // Ajouter les voisins de vtx dans la liste allowed
    for (int neighbor : G.neighbors(vtx)) {
        if (!IS.count(neighbor)) { // Ajouter seulement si le voisin n'est pas déjà dans IS
            allowed.push_back(neighbor);
        }
    }

    // Créer un nouvel ensemble indépendant Y à partir de IS et des sommets de allowed
    unordered_set<int> Y = IS;    // Copier IS dans Y
    IS_completion(G, Y, allowed); // Compléter gloutonnement pour maximiser Y

    // Si Y est plus grand que IS, remplacer IS par Y
    if (Y.size() > IS.size()) {
        IS = Y;
    } else {
        // Sinon, remettre vtx dans IS pour conserver l'ancien ensemble
        IS.insert(vtx);
    }
}

// Recherche locale
unordered_set<int> RL(Graph & G, int t) {
    // Initialisation : trouver un ensemble indépendant maximal par inclusion
    unordered_set<int> IS;  // Ensemble indépendant initial
    list<int> all_vertices; // Liste de tous les sommets du graphe

    for (int i = 0; i < G.get_size(); i++) {
        all_vertices.push_back(i);
    }

    // Construire gloutonnement un ensemble indépendant maximal
    IS_completion(G, IS, all_vertices);

    // Appliquer t étapes de RL_step
    for (int i = 0; i < t; i++) {
        // Sélectionner un sommet aléatoire de l'ensemble indépendant
        int vtx = sample(IS);

        // Exécuter une étape de recherche locale
        RL_step(G, IS, vtx);
    }

    return IS; // Retourner l'ensemble indépendant obtenu
}

// version déterministe
unordered_set<int> RL_det(Graph & G) {}

// Fonction de test
void RL_step_test(Graph & g, int t) {
    unordered_set<int> is = {};
    IS_completion(g, is, g.vertices());

    for (int i : is)
        g.set_color_mark(i, Graph::YELLOW); // les anciens sommets de l'IS sont en jaune

    unsigned int a = is.size();
    for (int i = 0; i < t; i++) {
        RL_step(g, is, sample(is));
        if (is.size() > a) {
            for (int i : is)
                g.set_shape_mark(i, Graph::SQUARE); // les nouveaux sommets sont carrés
            g.show();
            a = is.size();
        }
    }
}

/*****************/
/* RECUIT SIMULE */
/*****************/

// Une étape du recuit
unordered_set<int> RS_step(Graph & G, unordered_set<int> IS, int vtx) {
}

// Le recuit simulé
unordered_set<int> RS(Graph & G, int t) {
}

/*********************************/
/* RECHERCHE LOCALE À DISTANCE 2 */
/*********************************/

// Une étape de la recherche locale à distance 2
void RL_step2(Graph & G, unordered_set<int> & IS, int vtx) {
}

// la recherche locale
unordered_set<int> RL2(Graph & G, int t) {
}

/********/
/* MAIN */
/********/

int main() {
    srand(time(NULL));
    Graph g = Graph("data/graphEL_rand_1000_16");
    // g.set_int_mark(0, 10);
    // g.set_shape_mark(3, Graph::TRIANGLE);
    // g.set_color_mark(4, Graph::RED);
    // g.print();
    // g.show();

    /// Partie sur les algos gloutons:

    // std::unordered_set<int> test_glouton = greedy(g, 6000);
    // // std::unordered_set<int> test_glouton_det = greedy_det(g);

    // cout << "Algo glouton avec aléatoire: " << endl;
    // for (int i : test_glouton) {
    //     cout << i << " ";
    // }
    // cout << endl;

    // cout<<"Algo glouton déterministe: "<<endl;
    // for (int i : test_glouton_det) {
    //     cout << i << " ";
    // }
    // cout<<endl;




    ///Partie sur la recherche locale
    std::unordered_set<int> testlocal = RL(g, 60000);

    for (int i : testlocal) {
        cout << i << " ";
    }



    return 0;
}
