## Question 3. Que font les fonctions gen_perm et shuffle ?


Pour gen_perm:
Pour chaque index i, un entier aléatoire r est calculé dans l'intervalle [i, n-1] en utilisant l'expression i + rand() % (n - i).
Les valeurs res[i] et res[r] sont échangées (swap) pour introduire de l'aléatoire.
L'idée ici est une implémentation du Fisher-Yates Shuffle, qui garantit une permutation aléatoire uniforme.

en gros, on fait une permutation aléatoire d'un tableau 

Exemple avec n=4 :
Initialisation : res = {0, 1, 2, 3}.
Boucle :
i=0 : Choix aléatoire r=2, on échange res[0] et res[2] → res = {2, 1, 0, 3}.
i=1 : Choix aléatoire r=3, on échange res[1] et res[3] → res = {2, 3, 0, 1}.
i=2 : Choix aléatoire r=3, on échange res[2] et res[3] → res = {2, 3, 1, 0}.
i=3 : Pas d'échange nécessaire (dernière itération).
Résultat final : Une permutation aléatoire, par exemple {2, 3, 1, 0}.

Pour shuffle:

On prend un entrée un graphe et un tableau d'entier, les sommets du graphe deviennent les indices du tableau.

exemple:

Exemple avec un graphe :
Supposons un graphe G avec 4 sommets et les arêtes suivantes :
0 → 1
0 → 2
1 → 2
2 → 3

Permutation perm = {2, 3, 1, 0} :
Les sommets sont renumérotés comme suit :
0 → 2, 1 → 3, 2 → 1, 3 → 0

(Le sommet 0 devient le 2, le 1 devient le 3, etc..)

Transformation des arêtes dans H :
(0 → 1) devient (2 → 3)
(0 → 2) devient (2 → 1)
(1 → 2) devient (3 → 1)
(2 → 3) devient (1 → 0)

Nouveau graphe H :
2 → 3
2 → 1
3 → 1
1 → 0


Question 5. Tester votre algorithme sur les graphes de data/. Pour graphEL_rand_1000_16
et graphEL_roadNet-TX, quelles valeurs de t pouvez vous choisir pour répondre en
moins d’une minute ?


Pour graphEl_rand1000_16, j'ai tester t=6000
real    1m12.806s
user    1m12.583s
sys     0m0.001s



Question 9


En comparant RL et le glouton, pour un t à 6000, voici mes résultats avec RL
real    0m0.393s
user    0m0.392s
sys     0m0.001s

Donc vraiment beaucoup plus rapide


avec t=60000 j'ai eu
real    0m3.410s
user    0m3.409s
sys     0m0.000s

Bon j'ai la flemme d'aller plus loin mais c'est 200000 fois plus rapide (façon de parler)